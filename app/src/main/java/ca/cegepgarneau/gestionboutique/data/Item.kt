package ca.cegepgarneau.gestionboutique.data

data class Item(
    val id: String = "",
    val nom: String = "",
    val categorie: String = "",
    val prix: String = ""
)

