package ca.cegepgarneau.gestionboutique

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import ca.cegepgarneau.gestionboutique.databinding.ActivityAuthentificationBinding
import ca.cegepgarneau.gestionboutique.databinding.ActivityMainBinding
import ca.cegepgarneau.gestionboutique.ui.logins.ConnexionFragment
import ca.cegepgarneau.gestionboutique.ui.logins.CreerCompteFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class AuthentificationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAuthentificationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAuthentificationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navController = findNavController(R.id.nav_host_fragment_activity_authentifiction)
        setupActionBarWithNavController(navController)


    }
}
