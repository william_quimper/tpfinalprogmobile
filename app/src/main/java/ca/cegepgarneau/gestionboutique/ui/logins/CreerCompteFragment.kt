package ca.cegepgarneau.gestionboutique.ui.logins

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import ca.cegepgarneau.gestionboutique.R
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.auth

class CreerCompteFragment : Fragment() {
    private lateinit var auth: FirebaseAuth;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_creer_compte, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val createAccountButton = view.findViewById<Button>(R.id.buttonCreerComtpe)
        val emailEditText = view.findViewById<EditText>(R.id.editTextEmail)
        val passwordEditText = view.findViewById<EditText>(R.id.editTextPassword)
        val passwordEditText2 = view.findViewById<EditText>(R.id.editTextConfirmPassword)


        // Set up a click listener for your "Create Account" button
        createAccountButton.setOnClickListener {
            val email = emailEditText.text.toString()
            val password = passwordEditText.text.toString()
            val password2 = passwordEditText2.text.toString()

            if (email.isNotEmpty() && password.isNotEmpty()) {
                if (valideFormulaireInscription(password, email)) {
                    if (password2 == password) {

                        createAccount(email, password)
                        findNavController().navigate(R.id.action_navigation_inscription_to_navigation_connexion)
                    } else {
                        Toast.makeText(
                            context, "Les mots de passes ne correspondent pas",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            } else {
                Toast.makeText(
                    context, "Please enter email and password",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }


    private fun valideFormulaireInscription(password: String, email: String): Boolean {

        if (!valideEmail(email)) {
            Toast.makeText(context, "Courriel invalide (exemple@gmail.com)", Toast.LENGTH_SHORT)
                .show()
            return false
        }

        if (password.length < 6) {
            Toast.makeText(
                context,
                "Le mot de passe doit contenir au minimum 6 charactere",
                Toast.LENGTH_SHORT
            )
                .show()
            return false
        }

        return true
    }

    private fun createAccount(email: String, password: String) {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    // Account creation successful
                    Toast.makeText(
                        requireContext(),
                        "Compte créé",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    // If account creation fails, display a message to the user.
                    Toast.makeText(
                        requireContext(),
                        "Création du compte a échoué: ${task.exception?.message}",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }

    private fun valideEmail(email: String): Boolean {
        val emailRegex = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$"
        val pattern = Regex(emailRegex)
        return pattern.matches(email)
    }

}
