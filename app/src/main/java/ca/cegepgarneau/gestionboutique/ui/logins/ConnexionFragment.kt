package ca.cegepgarneau.gestionboutique.ui.logins

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import ca.cegepgarneau.gestionboutique.MainActivity
import ca.cegepgarneau.gestionboutique.R
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.auth.auth

class ConnexionFragment : Fragment() {
    private lateinit var auth: FirebaseAuth;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



    }

    private fun loginUser(email: String, password: String) {
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    // Login successful
                    Toast.makeText(requireContext(), "Login complété", Toast.LENGTH_SHORT).show()
                    // You can navigate to another activity or perform other actions here


                    // Create an Intent to start MainActivity
                    val intent = Intent(requireContext(), MainActivity::class.java)

                    // Start MainActivity
                    startActivity(intent)

                    // Finish the current activity
                    requireActivity().finish()
                } else {
                    // If login fails, display a message to the user based on the exception type.
                    when (val exception = task.exception) {
                        is FirebaseAuthInvalidUserException -> {
                            // This exception is thrown if the user account does not exist.
                            Toast.makeText(requireContext(), "Le compte n'existe pas.", Toast.LENGTH_SHORT).show()
                        }
                        is FirebaseAuthInvalidCredentialsException -> {
                            // This exception is thrown if the password is incorrect.
                            Toast.makeText(requireContext(), "Mot de passe incorrect.", Toast.LENGTH_SHORT).show()
                        }
                        else -> {
                            // Handle other authentication errors.
                            val errorMessage = exception?.message ?: "Erreur de connexion."
                            Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_SHORT).show()
                        }
                    }
                }

            }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_connexion, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Initialize Firebase Auth
        auth = FirebaseAuth.getInstance()

        val buttonLogin = view.findViewById<Button>(R.id.buttonLogin)

        val emailEditText = view.findViewById<EditText>(R.id.editTextEmail)
        val passwordEditText = view.findViewById<EditText>(R.id.editTextPassword)

        // Set up a click listener for your "Login" button
        buttonLogin.setOnClickListener {
            val email = emailEditText.text.toString()
            val password = passwordEditText.text.toString()

            if (email.isNotEmpty() && password.isNotEmpty()) {
                if(ValideEmail(email)){
                    loginUser(email, password)
                }else{
                    Toast.makeText(context, "Email invalide (exemple@gmail.com)", Toast.LENGTH_SHORT)
                        .show()
                }

            } else {
                Toast.makeText(context, "Veuillez entrer un email et un mot de passe", Toast.LENGTH_SHORT)
                    .show()
            }
        }

        // Set up a click listener for your "Create" button to navigate to the Inscription (Sign Up) fragment
        view.findViewById<Button>(R.id.buttonGoToSignUp).setOnClickListener {
            findNavController().navigate(R.id.action_navigation_connexion_to_navigation_inscription)
        }
    }


    fun ValideEmail(email: String): Boolean {
        val emailRegex = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$"
        val pattern = Regex(emailRegex)
        return pattern.matches(email)
    }
}