package ca.cegepgarneau.gestionboutique.ui.home

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ca.cegepgarneau.gestionboutique.R
import ca.cegepgarneau.gestionboutique.data.Item
import ca.cegepgarneau.gestionboutique.databinding.FragmentHomeBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import android.app.AlertDialog
import java.text.FieldPosition

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var auth: FirebaseAuth
    private lateinit var itemList: MutableList<Item>
    private lateinit var adapter: HomeAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        itemList = mutableListOf()

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val db = FirebaseFirestore.getInstance()

        db.collection("items").get().addOnSuccessListener { documents ->
                for (document in documents) {
                    val data = document.data
                    val idItem = document.id
                    val itemName = data.get("nom")
                    val category = data.get("categorie")
                    val price = data.get("prix")

                    val item = Item(idItem, itemName.toString(), category.toString(), price.toString())
                    itemList.add(item)
                }


                val recyclerView: RecyclerView = binding.recyclerView
                recyclerView.layoutManager = LinearLayoutManager(context)

                // Set l'adapter du recycler view avec l'interface des
                // clickListeners Modifier et Supprimer
                adapter = HomeAdapter(itemList, object : HomeAdapter.ItemClickListener {

                    /**
                     * Handler du click sur Modifier (un item)
                     */
                    override fun onModifyClicked(documentId: String, position: Int) {
                        val itemToModify = itemList.elementAt(position)
                        showModifyDialog(documentId, position, itemToModify)
                    }

                    /**
                     * Handler du click sur Supprimer (un item)
                     */
                    override fun onDeleteClicked(documentId: String) {
                        showDeleteConfirmationDialog(documentId)
                    }

                    /**
                     * Affiche le Dialog afin de modifier un Item
                     */
                    private fun showModifyDialog(
                        documentId: String, position: Int, itemToModify: Item
                    ) {
                        val builder = AlertDialog.Builder(requireContext())
                        builder.setTitle("Modifier Item")

                        val view = LayoutInflater.from(requireContext())
                            .inflate(R.layout.dialog_modify_item, null)
                        builder.setView(view)

                        val etNewName = view.findViewById<EditText>(R.id.tvName)
                        val spinnerNewCategory = view.findViewById<Spinner>(R.id.spinnerCategory)
                        val etNewPrice = view.findViewById<EditText>(R.id.tvPrice)

                        // Set le text du Nom et du prix selon l'item a modifier
                        etNewName.setText(itemToModify.nom)
                        etNewPrice.setText(itemToModify.prix)


                        // Set le spinner avec les catégories d'une boutique
                        val categories = arrayOf(
                            "Électronique", "Meubles", "Vêtements", "Articles de sport", "Outils"
                        )
                        val spinnerAdapter = ArrayAdapter(
                            requireContext(), android.R.layout.simple_spinner_item, categories
                        )
                        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        spinnerNewCategory.adapter = spinnerAdapter

                        builder.setPositiveButton("Modifier") { _, _ ->
                            val newName = etNewName.text.toString()
                            val newCategory = spinnerNewCategory.selectedItem.toString()
                            val newPrice = etNewPrice.text.toString()

                            // Methode modifierItem avec les nouvelles informations
                            modifyItem(documentId, newName, newCategory, newPrice, position)

                            // Notifie l'adapter qu'il y a eu du changement
                            adapterChanged()
                        }

                        builder.setNegativeButton("Annuler") { _, _ ->
                        }

                        builder.show()
                    }

                    /**
                     * Affiche une confirmation de supression d'un item
                     */
                    private fun showDeleteConfirmationDialog(documentId: String) {
                        val builder = AlertDialog.Builder(requireContext())
                        builder.setTitle("Delete Item")
                        builder.setMessage("Are you sure you want to delete this item?")

                        builder.setPositiveButton("Supprimer") { _, _ ->
                            // Call the method to delete the item from Firestore
                            deleteItem(documentId)
                        }

                        builder.setNegativeButton("Annuler") { _, _ ->
                            // Handle cancel action
                        }

                        builder.show()
                    }

                    /**
                     * Supprime un item de la base de donnée Firestore
                     */
                    private fun deleteItem(documentId: String) {
                        val db = FirebaseFirestore.getInstance()
                        val collectionReference = db.collection("items")

                        // Supprime un item dans la base de donnée
                        collectionReference.document(documentId).delete().addOnSuccessListener {
                                Log.d(TAG, "DocumentSnapshot successfully deleted!")

                                // Alerte l'adapter qu'il y a eu du changement
                                fetchUpdatedItemList()
                                adapterChanged()
                            }.addOnFailureListener { e ->
                                Log.w(TAG, "Error deleting document", e)
                            }
                    }

                    /**
                     * Modifie un item dans la base de donnée FireStore
                     */
                    private fun modifyItem(documentId: String, newName: String,
                        newCategory: String, newPrice: String, position: Int
                    ) {
                        // Connexion a la collection items de la BD
                        val db = FirebaseFirestore.getInstance()
                        val collectionReference = db.collection("items")

                        // Modifie l'item avec les nouvelles informations
                        collectionReference.document(documentId).update(
                                mapOf(
                                    "nom" to newName, "categorie" to newCategory, "prix" to newPrice
                                )
                            ).addOnSuccessListener {
                                // modifie l'item dans la liste local et notifie l'adapter qu'il y a eu un changement
                                Log.d(TAG, "DocumentSnapshot successfully updated!")
                                itemList[position] =
                                    Item(documentId, newName, newCategory, newPrice)
                                adapter.notifyItemChanged(position)
                            }.addOnFailureListener { e ->
                                Log.w(TAG, "Error updating document", e)
                            }
                    }
                })

                recyclerView.adapter = adapter
            }.addOnFailureListener { exception ->
                // Handle failures
                Log.w(TAG, "Error getting documents: ", exception)
            }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    /**
     *     Tentative de notifier l'adapter
     *     Lors de la suppression et la modification d'un item
     *     Afin de reload automatiquement
     *     TODO : Ne fonctionne pas
     */
    private fun adapterChanged() {
        fetchUpdatedItemList()
        adapter.notifyDataSetChanged()
    }


    /**
     * Obtiens tous les items de la BD Firestore
     */
    private fun fetchUpdatedItemList() {
        // Connexion a la bd
        val db = FirebaseFirestore.getInstance()
        val collectionReference = db.collection("items")

        // Obtiens les Items
        collectionReference.get().addOnSuccessListener { documents ->
                val updatedItemList = mutableListOf<Item>()

                // Ajoute chaque items dans la mutableList
                for (document in documents) {
                    val data = document.data
                    val idItem = document.id
                    val itemName = data["nom"] as? String
                    val category = data["categorie"] as? String
                    val price = data["prix"] as? String

                    if (itemName != null && category != null && price != null) {
                        val item = Item(idItem, itemName, category, price)
                        updatedItemList.add(item)
                    }
                }

                // Ajoute la mutableList à la list Local itemList
                itemList.clear()
                itemList.addAll(updatedItemList)
            }.addOnFailureListener { exception ->
                // Handle failures
                Log.w(TAG, "Error getting documents: ", exception)
            }
    }


}