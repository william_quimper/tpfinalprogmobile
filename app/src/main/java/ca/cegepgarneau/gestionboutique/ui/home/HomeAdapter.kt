package ca.cegepgarneau.gestionboutique.ui.home

import android.app.AlertDialog
import android.content.ContentValues.TAG
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.recyclerview.widget.RecyclerView
import ca.cegepgarneau.gestionboutique.R
import ca.cegepgarneau.gestionboutique.data.Item
import com.google.firebase.firestore.FirebaseFirestore



class HomeAdapter(
    private val itemList: List<Item>,
    private val itemClickListener: ItemClickListener) :
    RecyclerView.Adapter<HomeAdapter.ItemViewHolder>() {

    interface ItemClickListener {
        fun onModifyClicked(documentId: String, position: Int)
        fun onDeleteClicked(documentId: String)
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemNameTextView: TextView = itemView.findViewById(R.id.textViewNom)
        val categoryTextView: TextView = itemView.findViewById(R.id.textViewCategorie)
        val priceTextView: TextView = itemView.findViewById(R.id.textViewPrix)
        val ivLocation: ImageView = itemView.findViewById(R.id.iv_loc_cat)

        init {
            itemView.setOnClickListener {
                val position = adapterPosition
                showPopupMenu(itemView, position)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val currentItem = itemList[position]

        holder.itemNameTextView.text = currentItem.nom
        holder.categoryTextView.text = currentItem.categorie
        holder.priceTextView.text = currentItem.prix
        holder.ivLocation.setImageResource(R.drawable.img_electronique)

        when (currentItem.categorie){
            "Électronique" -> holder.ivLocation.setImageResource(R.drawable.img_electronique)
            "Meubles" -> holder.ivLocation.setImageResource(R.drawable.img_meuble)
            "Vêtements" -> holder.ivLocation.setImageResource(R.drawable.img_vetements)
            "Articles de sport" -> holder.ivLocation.setImageResource(R.drawable.img_sport)
            "Outils" -> holder.ivLocation.setImageResource(R.drawable.img_outil)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    private fun showPopupMenu(view: View, position: Int) {
        val popupMenu = PopupMenu(view.context, view)
        val inflater: MenuInflater = popupMenu.menuInflater
        inflater.inflate(R.menu.menu_items_option, popupMenu.menu)

        val currentItem = itemList[position]
        val documentId = currentItem.id

        popupMenu.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.menu_modify -> {
                    itemClickListener.onModifyClicked(documentId, position)
                    true
                }
                R.id.menu_delete -> {
                    itemClickListener.onDeleteClicked(documentId)
                    true
                }
                else -> false
            }
        }

        popupMenu.show()
    }
}


