    package ca.cegepgarneau.gestionboutique.ui.createItem

    import android.Manifest
    import android.app.NotificationChannel
    import android.app.NotificationManager
    import android.content.Context
    import android.content.pm.PackageManager
    import android.media.MediaPlayer
    import android.net.Uri
    import android.os.Build
    import android.os.Bundle
    import android.util.Log
    import android.view.LayoutInflater
    import android.view.View
    import android.view.ViewGroup
    import android.widget.ArrayAdapter
    import android.widget.Spinner
    import androidx.core.app.ActivityCompat
    import androidx.core.app.NotificationCompat
    import androidx.core.app.NotificationManagerCompat
    import androidx.core.content.ContextCompat
    import androidx.core.content.ContextCompat.getSystemService
    import androidx.fragment.app.Fragment
    import androidx.navigation.fragment.findNavController
    import ca.cegepgarneau.gestionboutique.R
    import ca.cegepgarneau.gestionboutique.databinding.FragmentDashboardBinding
    import com.google.firebase.firestore.FirebaseFirestore


    class CreateItemFragment : Fragment() {
        private var _binding: FragmentDashboardBinding? = null

        private val binding get() = _binding!!

        private var mediaPlayer: MediaPlayer? = null


        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View {

            _binding = FragmentDashboardBinding.inflate(inflater, container, false)
            val root: View = binding.root

            // Récupérer la référence du Spinner dans le onCreateView
            val spinnerCategory: Spinner = binding.spinnerCategory

            // Créer une liste d'options pour le Spinner
            val categories =
                arrayOf("Électronique", "Meubles", "Vêtements", "Articles de sport", "Outils")

            // Créer un ArrayAdapter en utilisant la liste d'options et le layout par défaut du Spinner
            val adapter = ArrayAdapter(requireContext(), R.layout.spinner_dropdown, categories)

            // Spécifier le layout à utiliser lorsque la liste apparaît
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            // Appliquer l'adaptateur au Spinner
            spinnerCategory.adapter = adapter

            return root
        }

        override fun onDestroyView() {
            super.onDestroyView()
            _binding = null
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            val db = FirebaseFirestore.getInstance()

            binding.buttonAddItem.setOnClickListener {

                val selectedCategorie = binding.spinnerCategory.selectedItem.toString()
                val selectedItemName = binding.editTextItemName.text.toString()
                val selectedPrice = binding.editTextPrice.text.toString()

                // Item a ajouter dans la Base de donné Firestore
                val itemData = hashMapOf(
                    "nom" to selectedItemName,
                    "categorie" to selectedCategorie,
                    "prix" to selectedPrice
                )

                // Ajout de l'item
                db.collection("items").add(itemData)

                mediaPlayer = MediaPlayer.create(requireContext(), R.raw.notification_sound);
                playSound();

                /**
                 * La notification ne fonctionne pas
                 */
//                createNotificationChannel()
//                showNotification()

                // Naviguer au fragment principal
                findNavController().navigate(R.id.action_navigation_dashboard_to_navigation_home)
            }

        }

        /**
         * Joue une petite track sonore
         */
        private fun playSound() {
            if (mediaPlayer != null) {
                mediaPlayer!!.seekTo(0) // Rewind to the beginning
                mediaPlayer!!.start()
            }
        }


//        private fun createNotificationChannel() {
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                val name = "Channel"
//                val descriptionText = "Channel Description"
//                val importance = NotificationManager.IMPORTANCE_DEFAULT
//
//                val channel = NotificationChannel(requireContext().getString(R.string.notification_channel_id), name, importance).apply {
//                    description = descriptionText
//                }
//
//                val notificationManager: NotificationManager =
//                    requireActivity().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//                notificationManager.createNotificationChannel(channel)
//            }
//        }

//        private fun showNotification() {
//            val builder = NotificationCompat.Builder(
//                requireContext(),
//                requireContext().getString(R.string.notification_channel_id)
//            )
//                .setSmallIcon(R.drawable.ic_notifications_black_24dp)
//                .setContentTitle("Item ajouté")
//                .setContentText("Votre item a été ajouté!")
//                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//
//            // Show the notification
//            with(NotificationManagerCompat.from(requireContext())) {
//                if (ActivityCompat.checkSelfPermission(
//                        requireContext(),
//                        Manifest.permission.POST_NOTIFICATIONS
//                    ) != PackageManager.PERMISSION_GRANTED
//                ) {
//                    notify(1, builder.build())
//                }
//            }
//        }
    }

